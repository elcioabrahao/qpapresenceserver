package br.com.qpainformatica.qpapresence.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.qpainformatica.qpapresence.model.Presenca;


@Repository
public class PresencaDAO {
	@PersistenceContext
	EntityManager manager;

	public void criar(Presenca presenca){
		manager.persist(presenca);
	}
	
	public void atualizar(Presenca presenca){
		System.out.println("vai atualizar: "+presenca);
		manager.merge(presenca);
	}
	
	public void excluir(Presenca presenca){
		manager.remove(manager.find(Presenca.class, presenca.getId()));
	}
	
	public Presenca selecionar(int id){
		return manager.find(Presenca.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Presenca> listarPresencas(){
		return manager.createQuery("select l from Presenca l").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Presenca> listarPresencas(String chave){
		String jpql = "select l from Presenca l where l.nome like :parte";
		Query query = manager.createQuery(jpql);
		query.setParameter("parte", "%"+chave+"%");
		List<Presenca> result = query.getResultList();
		return result;
	}
}
