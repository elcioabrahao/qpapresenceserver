package br.com.qpainformatica.qpapresence.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.qpainformatica.qpapresence.model.Presenca;
import br.com.qpainformatica.qpapresence.service.PresencaService;


@RestController
public class PresencaRest {
	private PresencaService ls;

	@Autowired
	public PresencaRest(PresencaService ls) {
		this.ls = ls;
	}

	@RequestMapping(method = RequestMethod.GET, value = "rest/presencas")
	public @ResponseBody List<Presenca> listagem(String chave) {
		List<Presenca> lista = null;
		try {
			if (chave == null || chave.equals("")) {
				lista = ls.listarPresencas();
			} else {
				lista = ls.listarPresencas(chave);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lista;
	}

	@RequestMapping(method = RequestMethod.GET, value = "rest/presencas/{id}")
	public @ResponseBody Presenca listaLocal(@PathVariable("id") Long id) {
		Presenca local = null, param;
		try {
			param = new Presenca();
			param.setId(id.intValue());
			local = ls.mostrar(param);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return local;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.POST, value = "rest/presenca")
	public ResponseEntity<Presenca> criarLocal(@RequestBody Presenca local) {
		try {
			ls.criar(local);
			return new ResponseEntity<Presenca>(local, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<Presenca>(local, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}