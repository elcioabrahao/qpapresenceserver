package br.com.qpainformatica.qpapresence.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import br.com.qpainformatica.qpapresence.model.Cidade;
import br.com.qpainformatica.qpapresence.model.Presenca;
import br.com.qpainformatica.qpapresence.model.Tipo;
import br.com.qpainformatica.qpapresence.service.CidadeService;
import br.com.qpainformatica.qpapresence.service.PresencaService;
import br.com.qpainformatica.qpapresence.service.TipoService;


@Transactional
@Controller
public class PresencaController {
	private TipoService ts;
	private CidadeService cs;
	private PresencaService ls;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	public PresencaController(TipoService ts, CidadeService cs,
			PresencaService ls) {
		this.ts = ts;
		this.cs = cs;
		this.ls = ls;
	}

	@RequestMapping("index")
	public String home(){
		return "redirect:listar_presencas";
	}
	
	@RequestMapping("nova_presenca")
	public String form(Model model) {

		try {
			List<Tipo> tipos = ts.listarTipos();
			model.addAttribute("tipos", tipos);
			List<Cidade> cidades = cs.listarCidades();
			model.addAttribute("cidades", cidades);
			return "presenca/presencacriar";

		} catch (IOException e) {
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}
	
	@RequestMapping("alterar_presenca")
	public String formAlterar(Model model, Presenca presenca) {
		try {
			List<Tipo> tipos = ts.listarTipos();
			model.addAttribute("tipos", tipos);
			List<Cidade> cidades = cs.listarCidades();
			model.addAttribute("cidades", cidades);
			model.addAttribute("presenca", ls.mostrar(presenca));
			return "presenca/presencaalterar";
		} catch (IOException e) {
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}

	@RequestMapping("incluir_presenca")
	public String inclusao(@Valid Presenca presenca, BindingResult result, Model model, @RequestParam("file") MultipartFile file) {
		
		try {
			if (result.hasErrors()) {
				List<Tipo> tipos = ts.listarTipos();
				model.addAttribute("tipos", tipos);
				List<Cidade> cidades = cs.listarCidades();
				model.addAttribute("cidades", cidades);
				return "presenca/presencacriar";
			}
			ls.criar(presenca);
			ls.gravarImagem(servletContext, presenca, file);
			return "redirect:listar_presencas";
		} catch (IOException e) {
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}
	
	@RequestMapping("listar_presencas")
	public String listagem(Model model, String chave) {
		try{
			if(chave == null || chave.equals("")){
				model.addAttribute("presencas", ls.listarPresencas());
			} else {
				model.addAttribute("presencas", ls.listarPresencas(chave));
			}
			return "presenca/presencalistar";
		} catch(IOException e){
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}
	
	@RequestMapping("limpar_presencas")
	public String limparListagem(Model model) {
		model.addAttribute("presencas", null);
		return "presenca/presencalistar";
	}
	
	@RequestMapping("mostrar_presenca")
	public String mostrar(Presenca presenca, Model model) {
		try{
			model.addAttribute("presenca",ls.mostrar(presenca));
			return "presenca/presencamostrar";
		} catch(IOException e){
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}
	
	@RequestMapping("remover_presenca")
	public String remover(Presenca presenca, Model model) {
		try{
			ls.remover(presenca);
			return "redirect:listar_presencas";
		} catch(IOException e){
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}
	
	@RequestMapping("atualizar_presenca")
	public String atualizar(Presenca presenca, Model model) {
		try{
			ls.atualizar(presenca);
			return "redirect:listar_presencas";
		} catch(IOException e){
			e.printStackTrace();
			model.addAttribute("erro",e);
		}
		return "erro";
	}
}
