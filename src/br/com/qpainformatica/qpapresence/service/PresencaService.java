package br.com.qpainformatica.qpapresence.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import br.com.qpainformatica.qpapresence.dao.CidadeDAO;
import br.com.qpainformatica.qpapresence.dao.EstadoDAO;
import br.com.qpainformatica.qpapresence.dao.PresencaDAO;
import br.com.qpainformatica.qpapresence.google.Localizacao;
import br.com.qpainformatica.qpapresence.google.ResultadoGoogle;
import br.com.qpainformatica.qpapresence.model.Cidade;
import br.com.qpainformatica.qpapresence.model.Estado;
import br.com.qpainformatica.qpapresence.model.Presenca;





@Service
public class PresencaService {
	
	private final String PAIS = "Brasil";
	private final String KEY = "key=AIzaSyCHmiv5wopeFDKhWJSwJJwK2FbnyrUAfxQ";
	private CidadeDAO cidadeDAO;
	private EstadoDAO estadoDAO;
	
	
	private PresencaDAO dao;
	
	@Autowired
	public PresencaService(PresencaDAO dao, CidadeDAO cidadeDAO,
			EstadoDAO estadoDAO) {
		this.dao = dao;
		this.cidadeDAO = cidadeDAO;
		this.estadoDAO = estadoDAO;
	}
	public void criar(Presenca presenca) throws IOException{
		dao.criar(presenca);
	}	
	public void remover(Presenca presenca) throws IOException{
		dao.excluir(presenca);
	}
	public void atualizar(Presenca presenca) throws IOException{
		dao.atualizar(presenca);
	}
	public List<Presenca> listarPresencas() throws IOException{
		return dao.listarPresencas();
	}
	public List<Presenca> listarPresencas(String chave) throws IOException {
		return dao.listarPresencas(chave);
	}
	public Presenca mostrar(Presenca presenca) throws IOException {
		return dao.selecionar(presenca.getId());
	}
	
	public Presenca preencherLacunas(Presenca presenca) {
		Cidade cidade = cidadeDAO.selecionar(presenca.getCidade().getId());
		presenca.setCidade(cidade);
		Estado estado = estadoDAO.selecionar(presenca.getCidade().getEstado().getId());
		presenca.getCidade().setEstado(estado);
		ResultadoGoogle resultado = buscaGoogleMaps(presenca);
		Localizacao presencaizacao = getLocalizacao(resultado);
		presenca.setLatitude(presencaizacao.getLat());
		presenca.setLongitude(presencaizacao.getLng());
		presenca.setLogradouro(getEnderecoFormatado(resultado));
		return presenca;
	}	
	
	private Localizacao getLocalizacao(ResultadoGoogle resultado) {
		return resultado.getLocalizacao();
	}

	private String montaURL(Presenca presenca){
		final String SEPARADOR = ",";
		String url = "https://maps.googleapis.com/maps/api/geocode/json";
		String address = "address="+presenca.getLogradouro()+SEPARADOR+
				presenca.getCidade().getNome()+SEPARADOR+presenca.getCidade().getEstado().getNome()+
				SEPARADOR+PAIS;
		return url+"?"+address+"&"+KEY;
	}

	private String getEnderecoFormatado(ResultadoGoogle resultado) {
		return resultado.getEnderecoFormatado();
	}
	
	private ResultadoGoogle buscaGoogleMaps(Presenca presenca) {
		RestTemplate restTemplate = new RestTemplate();
		ResultadoGoogle resultado = restTemplate.getForObject(
			montaURL(presenca), ResultadoGoogle.class);
		return resultado;
	}
	
	
	public void gravarImagem(ServletContext servletContext, Presenca presenca, MultipartFile file)
			throws IOException {
		if (!file.isEmpty()) {
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(file
					.getBytes()));
			String path = servletContext.getRealPath(servletContext	.getContextPath());
			path = path.substring(0, path.lastIndexOf(File.separatorChar));
			String nomeArquivo = "img"+presenca.getId()+".jpg";
			presenca.setImagem(nomeArquivo);
			atualizar(presenca);
			File destination = new File(path + File.separatorChar + "img" + File.separatorChar + nomeArquivo);
			if(destination.exists()){
				destination.delete();
			}
			ImageIO.write(src, "jpg", destination);
		}
	}
	
	
}
