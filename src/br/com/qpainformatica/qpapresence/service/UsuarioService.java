package br.com.qpainformatica.qpapresence.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qpainformatica.qpapresence.dao.UsuarioDAO;
import br.com.qpainformatica.qpapresence.model.Usuario;


@Service
public class UsuarioService {
	UsuarioDAO dao;
	@Autowired
	public UsuarioService(UsuarioDAO dao) {
		this.dao = dao;
	}

	public boolean validar(Usuario usuario) throws IOException{
		return dao.validar(usuario);
	}
}
