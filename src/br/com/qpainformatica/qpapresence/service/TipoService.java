package br.com.qpainformatica.qpapresence.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qpainformatica.qpapresence.dao.TipoDAO;
import br.com.qpainformatica.qpapresence.model.Tipo;


@Service
public class TipoService {
	private TipoDAO dao;
	@Autowired
	public TipoService(TipoDAO dao){
		this.dao = dao;
	}
	public List<Tipo> listarTipos() throws IOException{
		return dao.selecionarTodos();
	}
}
