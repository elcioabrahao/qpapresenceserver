package br.com.qpainformatica.qpapresence.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qpainformatica.qpapresence.dao.CidadeDAO;
import br.com.qpainformatica.qpapresence.model.Cidade;


@Service
public class CidadeService {
	private CidadeDAO dao;
	
	@Autowired
	public CidadeService(CidadeDAO dao){
		this.dao = dao;
	}
	public List<Cidade> listarCidades() throws IOException{
		return dao.selecionarTodas();
	}
}
