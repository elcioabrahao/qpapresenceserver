package br.com.qpainformatica.qpapresence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Entity
public class Presenca implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idLocal")
	private int id;
	
	@NotNull 
	@Size(max=128, min=5, message="O tamanho máximo do nome está entre 5 e 128 caracteres")
	@Column(name="nmLocal")
	private String nome;
	
	@NotNull 
	private double latitude;

	//incluindo Fernando de Noronha
	@NotNull 
	private double longitude;

	@Size(max=128, min=5, message="O tamanho máximo do nome da imagem está entre 5 e 128 caracteres")
	private String imagem;
	
	@NotNull 
	@Size(max=255, min=2, message="O tamanho máximo do logradouro está entre 2 e 255 caracteres")
	@Column(name="logradouro")
	private String logradouro;

	@NotNull 
	@Size(max=10, min=0, message="O tamanho máximo do numero está entre 0 e 10 caracteres")
	@Column(name="numero")
	private String numero;
	
	@Size(max=20, message="O tamanho máximo do complemento é 20 caracteres")
	@Column(name="complemento")
	private String complemento;

	@NotNull 
	@Size(max=128, message="O tamanho máximo deste campo é 128 caracteres")
	@Column(name="identificador1")
	private String identificador1;

	@Size(max=128, message="O tamanho máximo deste campo é 128 caracteres")
	@Column(name="identificador2")
	private String identificador2;
	
	@Size(max=128, message="O tamanho máximo deste campo é 128 caracteres")
	@Column(name="identificador3")
	private String identificador3;
	
	@NotNull 
	@Column(name="data_horario_inicio")
	private Date datahorarioinicio;
	
	@NotNull 
	@Column(name="data_horario_termino")
	private Date datahorariotermino;
		
	@NotNull
	@ManyToOne
	@JoinColumn(name="idCidade")
	private Cidade cidade;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="idTipoLocal")
	private Tipo tipo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	
	

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getIdentificador1() {
		return identificador1;
	}

	public void setIdentificador1(String identificador1) {
		this.identificador1 = identificador1;
	}

	public String getIdentificador2() {
		return identificador2;
	}

	public void setIdentificador2(String identificador2) {
		this.identificador2 = identificador2;
	}

	public String getIdentificador3() {
		return identificador3;
	}

	public void setIdentificador3(String identificador3) {
		this.identificador3 = identificador3;
	}


	public Date getDatahorarioinicio() {
		return datahorarioinicio;
	}

	public void setDatahorarioinicio(Date datahorarioinicio) {
		this.datahorarioinicio = datahorarioinicio;
	}

	public Date getDatahorariotermino() {
		return datahorariotermino;
	}

	public void setDatahorariotermino(Date datahorariotermino) {
		this.datahorariotermino = datahorariotermino;
	}

	@Override
	public String toString() {
		return "Local [id=" + id + ", nome=" + nome + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", imagem=" + imagem
				+ ", cidade=" + cidade + ", tipo=" + tipo + "]";
	}
	
}