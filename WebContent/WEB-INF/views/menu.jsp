<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="limpar_presencas">QPAPresence - Meus Eventos</a>
                <ul class="nav navbar-nav">
						<li><a href="listar_presencas">Eventos</a></li>
				</ul>
				<ul class="nav navbar-nav">
						<li><a href="controle_presencas">Presenças nos Eventos</a></li>
				</ul>
            </div>
            <c:if test="${usuarioLogado != null}">
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="logout">logout</a></li>
					</ul>
				</div>
		</c:if>
        </div>
    </nav>