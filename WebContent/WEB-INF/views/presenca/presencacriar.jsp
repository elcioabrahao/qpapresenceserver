<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="pt-br">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Novo Evento</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen"
     href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
<link href="css/style.css" rel="stylesheet">





</head>

<body>
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
	


	<!-- Barra superior com os menus de navegação -->
	<c:import url="../menu.jsp" />
	<!-- Container Principal -->
	<div id="main" class="container">
		<h3 class="page-header">Incluir Evento</h3>
		<!-- Formulario para inclusao de presencas -->
		<form action="incluir_presenca" method="post" enctype="multipart/form-data">
			<!-- area de campos do form -->
			<div class="row">
				<div class="form-group col-md-8">
					<label for="nome">Nome Evento</label> <input type="text"
						class="form-control" name="nome" id="nome" required
						maxlength="128" placeholder="Nome do Evento">
					<form:errors path="presenca.nome" cssStyle="color:red" />
				</div>
				<div class="form-group col-md-4">
					<label for="tipo">Tipo</label> <select name="tipo.id"
						class="form-control">
						<c:forEach var="tipo" items="${tipos}">
							<option value="${tipo.id}">${tipo.nome}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-4">
					<label for="logradouro">Logradouro</label> <input type="text"
						class="form-control" name="logradouro" id="logradouro" required
						maxlength="128" placeholder="Logradouro">
					<form:errors path="presenca.logradouro" cssStyle="color:red" />
				</div>
				<div class="form-group col-md-4">
					<label for="numero">Número</label> <input type="text"
						class="form-control" name="numero" id="numero" required
						maxlength="128" placeholder="Número">
					<form:errors path="presenca.numero" cssStyle="color:red" />
				</div>
				<div class="form-group col-md-4">
					<label for="cidade">Cidade: </label> <select id="cidade"
						name="cidade.id" class="form-control">
						<c:forEach items="${cidades}" var="cidade">
							<option value="${cidade.id}">${cidade.nome}-
								${cidade.estado.id}</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="form-group col-md-4">
					<label for="complemento">Complemento</label> <input type="text"
						class="form-control" name="complemento" id="complemento" required
						maxlength="128" placeholder="Complemento">
					<form:errors path="presenca.complemento" cssStyle="color:red" />
				</div>

			</div>


			<div class="row">

			<div class="form-group col-md-4">
				<div id="datahorarioinicio" class="input-append">
				<label for="datahorarioinicio">Data/Horário de Início</label>
					<input data-format="dd/MM/yyyy hh:mm" type="text" ></input> 
					<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"> </i>
					</span>
				</div>
			</div>
			
			<div class="form-group col-md-4">
				<div id="datahorariotermino" class="input-append">
				<label for="datahorariotermino">Data/Horário de Término</label>
					<input data-format="dd/MM/yyyy hh:mm" type="text" ></input> 
					<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"> </i>
					</span>
				</div>
			</div>
			
			<script type="text/javascript">
				$(function() {
					$('#datahorarioinicio').datetimepicker({
						language : 'pt-BR',
						pick12HourFormat : false
					});
					$('#datahorariotermino').datetimepicker({
						language : 'pt-BR',
						pick12HourFormat : false
					});
				});
			</script>


			</div>

			<div class="row">
				<div class="form-group col-md-4">
					<label for="identificador1">Identificador 1</label> <input type="text"
						class="form-control" name="identificador1" id="identificador1" required
						maxlength="128" placeholder="Identificador 1 para participante">
					<form:errors path="presenca.identificador1" cssStyle="color:red" />
				</div>			
			</div>

			<div class="row">
				<div class="form-group col-md-4">
					<label for="identificador2">Identificador 2</label> <input type="text"
						class="form-control" name="identificador2" id="identificador2" 
						maxlength="128" placeholder="Identificador 2 para participante">
					<form:errors path="presenca.identificador2" cssStyle="color:red" />
				</div>			
			</div>

			<div class="row">
				<div class="form-group col-md-4">
					<label for="identificador3">Identificador 3</label> <input type="text"
						class="form-control" name="identificador3" id="identificador3" 
						maxlength="128" placeholder="Identificador 3 para participante">
					<form:errors path="presenca.identificador3" cssStyle="color:red" />
				</div>			
			</div>
			
			<div class="row">
				<div class="form-group col-md-8">
			<label for="arquivo">Por favor, selecione um arquivo de imagem do tipo jpg para carregar</label> 
    	       <input type="file" name="file"  id="arquivo" class="file"/>
    	       </div>
    	       </div>

			<hr />
			<div id="actions" class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-primary">Salvar</button>
					<a href="index" class="btn btn-default">Cancelar</a>
				</div>
			</div>
		</form>
	</div>

</body>

</html>